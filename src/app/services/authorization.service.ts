import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from '../utils/User';
import { Api } from '../utils/api';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  

  constructor(private http: HttpClient, private router: Router ) { }

  login(user: Login, callback) {
    console.log(user)
    this.http.post<any>(Api.LOGIN, user).subscribe(res => {
        console.log(res);
        let token = res.token;
        localStorage.setItem("auth_key", token);
        localStorage.setItem("username", user.email);
        localStorage.setItem("role", res.role);
        
        callback();
        this.router.navigate(['/']);
      },
      err => {
        alert("Niepoprawne dane logowania")
      });
  }

  logOut() {
    localStorage.removeItem("auth_key");
    localStorage.removeItem("username");
    localStorage.removeItem("role");
  }

  isLoggedInAdmin() {
    return localStorage.getItem("auth_key") !== null && localStorage.getItem("role")=='admin' ;
  }
  isLoggedInUser() {
    return localStorage.getItem("auth_key") !== null &&  localStorage.getItem("role")=='user';
  }
  isLoggedIn() {
    return localStorage.getItem("auth_key") !== null ;
  }
  
}
