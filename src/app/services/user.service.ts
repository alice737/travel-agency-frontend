import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, Tour } from '../utils/models';
import { Api } from '../utils/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  tourInCart: Array<Tour>;
  addUser(user: User) {
    const emailInUse = this.getOneUser(user.email).subscribe;
    if (emailInUse) {
      return console.error("Podany email");
    } else {
      this.http.post(Api.USER, user)
        .subscribe(
          (val) => {
            console.log("POST call successful value returned in body",
              val);
          },
          response => {
            console.log("POST call in error", response);
          },
          () => {
            console.log("The POST observable is now completed.");
          });
    }

  }




  getOneUser(email: String): Observable<User> {
    return this.http.get<User>(Api.GETONEUSER + "/" + email)
  }

}
