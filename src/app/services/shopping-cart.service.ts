import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Tour } from '../utils/models';
import { Api } from '../utils/api';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  constructor(private http: HttpClient) { }
  public tourInCart: Array<Tour> = [];
  public tourInCartWrapper: Array<CartWrapper> = []
  addTourToCart(tour: Tour): void {
    //to do implement with server   
    this.http.post(Api.ADDTOCART + "/"+localStorage.getItem("username"), tour)
      .subscribe(
        (val) => {
          console.log("POST call successful value returned in body",
            val);
        },
        response => {
          console.log("POST call in error", response);
        },
        () => {
          console.log("The POST observable is now completed.");
        });

    this.tourInCart.push(tour);
let notInArry =true;
    if (this.tourInCartWrapper.length > 0 ) {
      this.tourInCartWrapper.forEach(wrap => {
        if (wrap.tour._id == tour._id) {
          wrap.counter = wrap.counter + 1;
          notInArry=false;
        } 
      });
    } else {
      this.tourInCartWrapper.push(new CartWrapper(1, tour))
      notInArry=false;
    }

    if(notInArry){
      this.tourInCartWrapper.push(new CartWrapper(1, tour))

    }
    console.log(this.tourInCartWrapper)

  }

  removeTourInCart(tourToRemove: Tour) {
    let wasMatched =true;

    this.tourInCart = new  Array<Tour>();
    console.log(this.tourInCart)
    this.tourInCartWrapper.forEach(wrap => {
      if (wrap.tour._id == tourToRemove._id && wrap.counter>0 && wasMatched===true) {
        wrap.counter = wrap.counter - 1;
        wasMatched=false;
        let i :number;
        for(i=0;i<wrap.counter;i++){
          this.tourInCart.push(wrap.tour) 
        }
      } else{
        console.log("push")
        let j :number;
        for(j=0;j<wrap.counter;j++){
          this.tourInCart.push(wrap.tour) 
        }
      }
    });
  }

  getNumberOfTours(): number {
    return this.tourInCart.length;
  }

  getToursInCart() {
    return this.tourInCart;
  }


}
export class CartWrapper {
  constructor(public counter: number,
    public tour: Tour) {
  }
}

