import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { Order } from '../utils/models';
import { Api } from '../utils/api';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrderService {


  constructor(private http: HttpClient) { }

  getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(Api.ORDER)
  }
  addOrder(order: Order) {
    this.http.post(Api.ORDER+'/'+localStorage.getItem("username"), order)
      .subscribe(
        (val) => { console.log("POST call successful value returned in body", val); },
        response => { console.log("POST call in error", response); },
        () => { console.log("The POST observable is now completed."); });
  }
  getOrdersByEmail(): Observable<Order[]> {
    return this.http.get<Order[]>(Api.ORDER + "/" + localStorage.getItem("username"))
  }

}
