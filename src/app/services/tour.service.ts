import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Api} from '../utils/api'
import { Tour , Attraction} from '../utils/models';
import { Observable, of } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TourService {

  constructor(private http: HttpClient) { }
  private log(message: string) {
    
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }
  getAttraction(): Observable<Attraction[]>{
return this.http.get<Attraction[]>('http://localhost:8080/locations')
  }
  getTours (): Observable<Tour[]> {
    return this.http.get<Tour[]>(Api.TOURS)
  }
  getTours2 (): Observable<Tour[]> {
    return this.http.get<Tour[]>(Api.TOURS)
      .pipe(
        tap(_ => this.log('fetched getTours2')),
        catchError(this.handleError<Tour[]>('getTours2', []))
      );
  }
  updateTour (tour: Tour): Observable<any> {
    return this.http.put(Api.TOURS+`/${tour._id}`, tour).pipe(
      tap(_ => this.log(`updated tour id=${tour._id}`)),
      catchError(this.handleError<any>('updateTour'))
    );
  }
  deleteTour(tour: Tour): Observable<any>{
    return this.http.delete(Api.TOURS+`/${tour._id}`).pipe(
      tap(_ => this.log(`delete tour id=${tour._id}`)),
      catchError(this.handleError<any>('deleteTour'))
    );
  }

  addTour(tour:Tour) {
    this.http.post(Api.TOURS, tour)
        .subscribe(
            (val) => {
                console.log("POST call successful value returned in body", 
                            val);
            },
            response => {
                console.log("POST call in error", response);
 },
            () => {   console.log("The POST observable is now completed."); });
    }
  
}
