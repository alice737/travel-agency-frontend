import { Component, OnInit } from '@angular/core';
import { Tour } from '../../utils/models'
import { TourService } from 'src/app/services/tour.service'
import { UserService } from 'src/app/services/user.service'
import { Filter } from 'src/app/utils/Filter';
import { User } from 'src/app/utils/models';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
@Component({
  selector: 'app-tour-list',
  templateUrl: './tour-list.component.html',
  styleUrls: ['./tour-list.component.scss']
})
export class TourListComponent implements OnInit {
  p:number=1;
  config: any;
  tours: Tour[];
  min: number;
  max: number;
  filter : Filter;
  user: User;
  constructor(private tourService: TourService, private userService: UserService, private shoppingService : ShoppingCartService) { }
  ngOnInit() {
    this.tourService.getAttraction().subscribe(data => {
      console.log(data)
    });
    
    this.getTours();
    this.filter = new Filter(0, 0, new Date(), new Date(), 0,0, "");
this.getFilteredTours();
  }

  getTours(): void {
    this.min=0;
    this.max=0;
    this.tourService.getTours()
      .subscribe(tours => {

        this.tours = tours;
        this.tours.forEach(tour => {
          if (tour.price > this.max) {
            this.max = tour.price;
          }
        });
        this.tours.forEach(tour => {
          if (tour.price < this.min) {
            this.min = tour.price;
          }
        });

      });
  }

  goBack(): void {
    throw new Error("Method not implemented.");
  }
  pageChanged(event) {
    this.config.currentPage = event;
  }
  delete(tour: Tour): void {
    this.tourService.deleteTour(tour).subscribe();
    this.tours = this.tours.filter(item => item !== tour);
  }
  handleAddToCart(tour: Tour) {

    this.shoppingService.addTourToCart(tour);

  }
  getMaxPriceOfTours(): number {
    return this.max;
  }

  getMinPriceOfTours(): number {
    return this.min;
  }
  private getFilteredTours() {
    this.tourService.getTours()
      .subscribe(
        tours => this.tours =
        tours.filter(tour => (this.filter.tourCity== "" || tour.cityName == this.filter.tourCity)
          && ((this.filter.priceFrom == 0 && this.filter.priceTo == 0) || (tour.price > this.filter.priceFrom && tour.price < this.filter.priceTo))
          && ((this.filter.markFrom == 0 && this.filter.markTo == 0) || (tour.starCapacity > this.filter.markFrom && tour.starCapacity < this.filter.markTo))
    //       && ((new Date(this.filter.dateFrom) == new Date() && new Date(this.filter.dateTo) == new Date()) || (new Date(tour.startDate) > new Date(this.filter.dateFrom) && new Date(tour.endDate) < new Date(this.filter.dateTo)))

          ),
        error => console.log(error),
        
      );
  }
  handleCityChoice(filter: Filter): void {
     this.filter = filter;
     this.getFilteredTours();
  }
}
