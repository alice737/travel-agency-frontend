import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Login } from 'src/app/utils/User';
import { AuthorizationService } from 'src/app/services/authorization.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authorizationService : AuthorizationService) { }

  ngOnInit() {
  }
  loginForm= new FormGroup({
    
    email : new FormControl('', Validators.required),
    password : new FormControl('', Validators.required),

  });
  login() {
    let user = new Login(this.loginForm.value.email, this.loginForm.value.password);
    this.authorizationService.login(user, ()=> {
   
    });
  }
}
