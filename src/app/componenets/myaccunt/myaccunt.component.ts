import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';
import { Order } from 'src/app/utils/models';

@Component({
  selector: 'app-myaccunt',
  templateUrl: './myaccunt.component.html',
  styleUrls: ['./myaccunt.component.scss']
})
export class MyaccuntComponent implements OnInit {
orders:Order[]
  constructor(private orderService: OrderService) { }

  ngOnInit() {
    this.getOrdersByEmail()
  }
  getOrdersByEmail(){
     this.orderService.getOrdersByEmail().subscribe(order => {
      this.orders = order;
    });
  }
}
