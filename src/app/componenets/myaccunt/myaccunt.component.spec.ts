import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyaccuntComponent } from './myaccunt.component';

describe('MyaccuntComponent', () => {
  let component: MyaccuntComponent;
  let fixture: ComponentFixture<MyaccuntComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyaccuntComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyaccuntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
