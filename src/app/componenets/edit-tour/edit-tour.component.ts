import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tour } from 'src/app/utils/models';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-tour',
  templateUrl: './edit-tour.component.html',
  styleUrls: ['./edit-tour.component.scss']
})
export class EditTourComponent implements OnInit {
  public get authorizationService(): AuthorizationService {
    return this._authorizationService;
  }
  public set authorizationService(value: AuthorizationService) {
    this._authorizationService = value;
  }
  @Input() tour: Tour;
  @Output() notifyEditTour :EventEmitter<Tour> = new EventEmitter<Tour>();

  constructor(private _authorizationService: AuthorizationService) { }
  tripForm= new FormGroup({
    imageSrc : new FormControl('', Validators.required),
    cityName : new FormControl('', Validators.required),
    countryName : new FormControl('', Validators.required),
    hotelName : new FormControl('', Validators.required),
    description : new FormControl('', Validators.required),
    price : new FormControl('', Validators.required),
    startDate : new FormControl('', Validators.required),
    endDate : new FormControl('', Validators.required),
    numberSeats : new FormControl('', Validators.required),

  });
  ngOnInit() {
  }

  editTour(){

    const newTrip :Tour={
      imageSrc: this.tripForm.value.imageSrc,
      cityName: this.tripForm.value.cityName,
      countryName: this.tripForm.value.countryName,
      hotelName:this.tripForm.value.hotelName,
      description: this.tripForm.value.description,
      price: this.tripForm.value.price,
      startDate: this.tripForm.value.startDate,
      endDate: this.tripForm.value.endDate,
      numberSeats: this.tripForm.value.numberSeats,
      numberReservedSeats: 0,
      starCapacity: 0,
      comments: this.tour.comments
    };
    this.notifyEditTour.emit(newTrip as Tour);
  }
}
