import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/utils/models';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
   
  registerForm= new FormGroup({
    surname:  new FormControl('', Validators.required), 
    name : new FormControl('', Validators.required),
    email : new FormControl('', Validators.required),
    password : new FormControl('', Validators.required),
    role: new FormControl('', Validators.required)


  });
  constructor(private userService :UserService) { }

  ngOnInit() {
  }
  register(){
    const newUser :User={
      email: this.registerForm.value.email,
      name: this.registerForm.value.name,
      surname:this.registerForm.value.surname,
      password: this.registerForm.value.password,
      ordersIds: [],
      cartToursIds: [], 
      role: 'user'

    };
    this.userService.addUser(newUser as User);
  }
}
