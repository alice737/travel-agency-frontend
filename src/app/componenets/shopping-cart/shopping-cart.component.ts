import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service'
import { Tour } from 'src/app/utils/models';
import { TourService } from 'src/app/services/tour.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {

  constructor(private shoppingCartService: ShoppingCartService, private updater: TourService) {
  }

  sumPrices: number = 0;
  ngOnInit() {

  }
  getNumberOfTour(): number {
    return this.shoppingCartService.getNumberOfTours();
  }

  getToursInCart() {
    this.getTotalPrice();
    return this.shoppingCartService.getToursInCart()

  }
  getTotalPrice() {
    this.sumPrices = this.shoppingCartService.getToursInCart().reduce((sum, tour) => sum + tour.price, 0);
  }
  removeTour(tourToRemove: Tour) {
    this.shoppingCartService.removeTourInCart(tourToRemove);
    this.resignReservedSeat(tourToRemove);
  }
  resignReservedSeat(tour: Tour) {
    tour.numberReservedSeats = tour.numberReservedSeats - 1;
    this.updater.updateTour(tour)
      .subscribe();
  }


}
