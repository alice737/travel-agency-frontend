import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInfoCartComponent } from './modal-info-cart.component';

describe('ModalInfoCartComponent', () => {
  let component: ModalInfoCartComponent;
  let fixture: ComponentFixture<ModalInfoCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInfoCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInfoCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
