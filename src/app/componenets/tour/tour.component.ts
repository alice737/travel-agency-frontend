import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tour } from '../../utils/models'
import { TourService } from 'src/app/services/tour.service';
import { FormControl, Validators } from '@angular/forms';
import { AuthorizationService } from 'src/app/services/authorization.service';

@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.scss']
})
export class TourComponent implements OnInit {
  public get authorizationService(): AuthorizationService {
    return this._authorizationService;
  }
  public set authorizationService(value: AuthorizationService) {
    this._authorizationService = value;
  }

  // tours: Tour[];
  show: boolean = true;
  showR: boolean = false;
  messageReserved: string = "";
  messageResign: string = "";
  constructor(private tourService: TourService, private _authorizationService: AuthorizationService) {

  }
  @Input() tour: Tour;
  @Input() max: number;
  @Input() min: number;
  @Output() notify: EventEmitter<Tour> = new EventEmitter<Tour>();
  @Output() notifyAddToCart: EventEmitter<Tour> = new EventEmitter<Tour>();
  selected = 0;
  handleAddToCart() {
    this.notifyAddToCart.emit(this.tour);
    this.reservedSeats(this.tour)

  }

  handleChangeRate(tour: Tour) {
    console.log(tour)
    tour.starCapacity = (tour.starCapacity + this.selected) / 2;
    this.selected = tour.starCapacity;
    console.log(this.selected)
    console.log(this.selected)
    this.save(tour);

  }
  ngOnInit() {
    if (this.tour.numberSeats === this.tour.numberReservedSeats) {
      this.show = false;
      this.messageReserved = "Nie można zarezerwować miejsca ponieważ wszystkie są zarezerwowane"
    }

    // this.getTours();
  }

  clickCount(tour: Tour): void {
    // tour.numberSeats++;
    if (tour.numberReservedSeats > 0) {
      tour.numberReservedSeats--;
      this.save(tour);
      this.messageResign = ""
      this.showR = true;
      if (tour.numberReservedSeats == 0) {
        this.showR = false;
      }
    }
    else {
      this.showR = false;
      this.messageResign = "nie można zrezygnować miejsca ponieważ nie ma żadnej rezerwacji "
    }
  }
  reservedSeat(tour: Tour): void {
    if (tour.numberSeats > tour.numberReservedSeats) {
      // tour.numberSeats--;
      tour.numberReservedSeats++;
      this.messageReserved = "";
      this.messageResign = "";
      this.save(tour);
      this.show = true;
      this.showR = true;
    } else {
      this.show = false;
      this.messageReserved = "Nie można zarezerwować miejsca ponieważ wszystkie są zarezerwowane"
    }
  }
  save(tour: Tour): void {
    this.tourService.updateTour(tour)
      .subscribe();
  }
  delete() {
    this.notify.emit(this.tour);
  }



  goBack(): void {
    throw new Error("Method not implemented.");
  }

  reservedSeats(tour: Tour): void {
    if (tour.numberSeats > tour.numberReservedSeats) {
      tour.numberReservedSeats = tour.numberReservedSeats + 1;
    } else {
      this.show = false;
      this.messageReserved = "Nie można zarezerwować miejsca ponieważ wszystkie są zarezerwowane"
    }

    this.save(tour);
  }

  resignSeat(): void {

  }

  editTour(tour: Tour) {
    console.log(tour)
    this.tourService.addTour(tour);
  }

}
