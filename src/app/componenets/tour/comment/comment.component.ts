import { Component, OnInit, Input } from '@angular/core';
import { TourService } from 'src/app/services/tour.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OrderService } from 'src/app/services/order.service';
import { Order, Tour } from 'src/app/utils/models';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
  @Input() tour: Tour;

  comments = [
    {
      name: "anna@wp.pl",
      comment: "Było superrr"
    },
    {
      name: "ala@wp.pl",
      comment: "Mega"
    }

  ]
  commentForm = new FormGroup({
    newComment: new FormControl('', Validators.required),


  });
  orders:Order[]
  constructor(private orderService: OrderService) { }

  ngOnInit() {
 //   this.getOrdersByEmail()
  }
  getOrdersByEmail(){
     this.orderService.getOrdersByEmail().subscribe(order => {
      this.orders = order;
    });
      console.log(this.orders)

  }
  addComment() {
    const newComment = {
      name: localStorage.getItem("username"),
      comment: this.commentForm.value.newComment
    };
    this.comments.push(newComment);
  }
  checkIfUserCanAddComment() {

    let userCanAddComment = false;
    if (this.checkThatUserOrderThisTour() && this.checkIfUserDoNotAddComment()) {
      userCanAddComment = true;
    }
    console.log(userCanAddComment)
    return userCanAddComment;
  }

  checkThatUserOrderThisTour() {
    let tourWasReserved = false;
    if (localStorage.getItem("username").length > 0) {
    
      this.orders.forEach(order => {
        order.orderedToursIds.forEach(tour => {
          if (tour._id == this.tour._id) {
            tourWasReserved = true;
          }
        })
      })
    }
    return tourWasReserved;
  }
  checkIfUserDoNotAddComment() {
    let userNotCommented = true;
    this.tour.comments.forEach(comment => {
      if (comment.username == localStorage.getItem("username")) {
        userNotCommented = false;
      }
    })
    return userNotCommented;
  }

}
