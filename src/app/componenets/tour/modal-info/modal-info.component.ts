import { Component, OnInit, Input } from '@angular/core';
import { Tour } from 'src/app/utils/models';

@Component({
  selector: 'app-modal-info',
  templateUrl: './modal-info.component.html',
  styleUrls: ['./modal-info.component.scss']
})
export class ModalInfoComponent implements OnInit {
  @Input() tour: Tour;
  constructor() { }

  ngOnInit() {
  }

}
