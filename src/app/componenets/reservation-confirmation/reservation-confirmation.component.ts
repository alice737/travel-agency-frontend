import { Component, OnInit } from '@angular/core';
import { Tour, Order } from 'src/app/utils/models';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-reservation-confirmation',
  templateUrl: './reservation-confirmation.component.html',
  styleUrls: ['./reservation-confirmation.component.scss']
})
export class ReservationConfirmationComponent implements OnInit {

  constructor(private shoppingCartService: ShoppingCartService, private orderService: OrderService) {


  }

 sumPrices: number=0;
 ngOnInit() {
   
 }
 getNumberOfTour(): number {
   return this.shoppingCartService.getNumberOfTours();
 }
 // sumOfTour():void {
 //  this.toursInCart.map( it => {this.sumPrices=this.sumPrices+it.price; })
 //  }
 getToursInCart() {
   this.getTotalPrice();
 return this.shoppingCartService.getToursInCart()

 }
 getTotalPrice() {
   this.sumPrices = this.shoppingCartService.getToursInCart().reduce((sum,tour) => sum + tour.price, 0);
 }
 removeTour(tourToRemove: Tour) {
   this.shoppingCartService.removeTourInCart(tourToRemove);
   this.resignReservedSeat(tourToRemove);
 }
 resignReservedSeat(tour: Tour){
   tour.numberReservedSeats =  tour.numberReservedSeats-1;
 
 }
 addToursToOrder(){
   const newOrder: Order =
   {
    email: localStorage.getItem("username"),
    date: new Date(),
    orderedToursIds: this.getToursInCart(),
    isCompleted : true
   };
   this.orderService.addOrder(newOrder) 
 }
}
