import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm, FormGroup, Validators } from '@angular/forms';
import { TourService } from 'src/app/services/tour.service';
import { Tour } from 'src/app/utils/models';

@Component({
  selector: 'app-add-tour',
  templateUrl: './add-tour.component.html',
  styleUrls: ['./add-tour.component.scss']
})
export class AddTourComponent implements OnInit {

  tripForm= new FormGroup({
    imageSrc : new FormControl('', Validators.required),
    cityName : new FormControl('', Validators.required),
    countryName : new FormControl('', Validators.required),
    hotelName : new FormControl('', Validators.required),
    description : new FormControl('', Validators.required),
    price : new FormControl('', Validators.required),
    startDate : new FormControl('', Validators.required),
    endDate : new FormControl('', Validators.required),
    numberSeats : new FormControl('', Validators.required),

  });

  constructor(private tourService: TourService) { }

  ngOnInit() {
  }

  addNewTour(){

    const newTrip :Tour={
      imageSrc: this.tripForm.value.imageSrc,
      cityName: this.tripForm.value.cityName,
      countryName: this.tripForm.value.countryName,
      hotelName:this.tripForm.value.hotelName,
      description: this.tripForm.value.description,
      price: this.tripForm.value.price,
      startDate: this.tripForm.value.startDate,
      endDate: this.tripForm.value.endDate,
      numberSeats: this.tripForm.value.numberSeats,
      numberReservedSeats: 0,
      starCapacity: 0,
      comments: []
    };
    this.tourService.addTour(newTrip as Tour);
  }
   

}
