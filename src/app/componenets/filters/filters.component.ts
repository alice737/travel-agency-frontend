import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Filter } from 'src/app/utils/Filter';
import { TourService } from 'src/app/services/tour.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  cities: Set<String>;

  @Output() notify : EventEmitter<Filter> = new EventEmitter<Filter>();
  filter : Filter = new Filter(0, 0, new Date(), new Date(), 0, 0, "");

  constructor(private tourService: TourService) { }

  ngOnInit() {
    this.getToursCities();
   
  }



  private getToursCities() {
    this.tourService.getTours()
      .subscribe(tours => {
        this.cities = new Set(tours.map(tour => tour.cityName));
      });
  }

  handleCityChoice(city: string) {
    this.filter.tourCity = city;
    console.log(city)
    this.notify.emit(this.filter);
  }

  handlePriceRange(value) {
    this.filter.priceFrom = value.priceFrom;
    this.filter.priceTo = value.priceTo;
    this.notify.emit(this.filter);
  }
  handleDateRange(value) {
    this.filter.dateFrom = value.dateFrom;
    this.filter.dateTo = value.dateTo;
    this.notify.emit(this.filter);
  }
  handleMarkRange(value) {
    this.filter.markFrom = value.markFrom;
    this.filter.markTo = value.markTo;
    this.notify.emit(this.filter);
  }


  resetFilters() {
    this.filter = new Filter(0, 0, new Date(), new Date(), 0,0, "");
    this.notify.emit(this.filter);
  }

}
