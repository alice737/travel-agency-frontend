import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-date-filter',
  templateUrl: './date-filter.component.html',
  styleUrls: ['./date-filter.component.scss']
})
export class DateFilterComponent implements OnInit {
  @Output() notify: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }
  public dateFrom: number;
  public dateTo: number;
  ngOnInit() {
  }
  sendDate() {
    let dateFrom = (this.dateFrom) ? this.dateFrom : new Date(2019, 1, 1);
    let dateTo = (this.dateTo) ? this.dateTo : new Date();
    this.notify.emit({ dateFrom: dateFrom, dateTo: dateTo });
  }
}
