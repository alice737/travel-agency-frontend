import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tour-country-filter',
  templateUrl: './tour-country-filter.component.html',
  styleUrls: ['./tour-country-filter.component.scss']
})
export class TourCountryFilterComponent implements OnInit {
  ngOnInit(): void {
  }

  @Input() city: String;
  @Output() notify : EventEmitter<String> = new EventEmitter<String>();

  handleClick() {
    this.notify.emit(this.city);
  }

}
