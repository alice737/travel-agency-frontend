import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourCountryFilterComponent } from './tour-country-filter.component';

describe('TourCountryFilterComponent', () => {
  let component: TourCountryFilterComponent;
  let fixture: ComponentFixture<TourCountryFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourCountryFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourCountryFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
