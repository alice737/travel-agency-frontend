import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-mark-filter',
  templateUrl: './mark-filter.component.html',
  styleUrls: ['./mark-filter.component.scss']
})
export class MarkFilterComponent implements OnInit {
  @Output() notify : EventEmitter<any> = new EventEmitter<any>();
  constructor() { }
  public markFrom : number;
  public markTo : number;

  sendMark() {
    let markFrom =  (this.markFrom) ? this.markFrom : 0;
    let markTo =  (this.markTo) ? this.markTo : 5;
    this.notify.emit({markFrom : markFrom, markTo: markTo});
  }
  ngOnInit() {
  }

}
