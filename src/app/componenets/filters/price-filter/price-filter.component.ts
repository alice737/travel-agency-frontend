import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-price-filter',
  templateUrl: './price-filter.component.html',
  styleUrls: ['./price-filter.component.scss']
})
export class PriceFilterComponent implements OnInit {

  @Output() notify : EventEmitter<any> = new EventEmitter<any>();
  constructor() { }
  public priceFrom : number;
  public priceTo : number;

  sendPrice() {
    let priceFrom =  (this.priceFrom) ? this.priceFrom : 0;
    let priceTo =  (this.priceTo) ? this.priceTo : Number.MAX_VALUE;
    this.notify.emit({priceFrom : priceFrom, priceTo: priceTo});
  }

  ngOnInit() {
  }

}
