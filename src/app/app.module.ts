import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NavComponent } from './componenets/nav/nav.component';
import { TourComponent } from './componenets/tour/tour.component';
import { FooterComponent } from './componenets/footer/footer.component';
import { TourListComponent } from './componenets/tour-list/tour-list.component';
import { HttpClientModule } from '@angular/common/http';
import { ShoppingCartComponent } from './componenets/shopping-cart/shopping-cart.component';
import { AddTourComponent } from './componenets/add-tour/add-tour.component';
import { Routes } from '@angular/router';
import { ModalInfoComponent } from './componenets/tour/modal-info/modal-info.component';
import { ModalInfoCartComponent } from './componenets/shopping-cart/modal-info-cart/modal-info-cart.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {ShoppingCartService} from 'src/app/services/shopping-cart.service'
import {TourService} from 'src/app/services/tour.service'
import { RouterModule } from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CommentComponent } from './componenets/tour/comment/comment.component';
import { LoginComponent } from './componenets/login/login.component';
import { RegisterComponent } from './componenets/register/register.component';
import { FormsModule } from '@angular/forms';
import { FiltersComponent } from './componenets/filters/filters.component';
import { PriceFilterComponent } from './componenets/filters/price-filter/price-filter.component';
import { DateFilterComponent } from './componenets/filters/date-filter/date-filter.component';
import { MarkFilterComponent } from './componenets/filters/mark-filter/mark-filter.component';
import { TourCountryFilterComponent } from './componenets/filters/tour-country-filter/tour-country-filter.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminPanelComponent } from './componenets/admin-panel/admin-panel.component';
import { EditTourComponent } from './componenets/edit-tour/edit-tour.component';
import { ReservationConfirmationComponent } from './componenets/reservation-confirmation/reservation-confirmation.component';
import {UserService} from 'src/app/services/user.service';
import { OrderHistoryComponent } from './componenets/order-history/order-history.component'
import { AuthorizationGuardUser } from './guards/AuthorizationGuardUser';
import { AuthorizationGuardAdmin } from './guards/AuthorizationGuardAdmin';
import { MyaccuntComponent } from './componenets/myaccunt/myaccunt.component';
import { AuthorizationService } from './services/authorization.service';
import { OrderService } from './services/order.service';
const appRoutes: Routes = [
  { path: '', component: TourListComponent },
  { path: 'addNewTour', component: AddTourComponent},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent }, 
  { path: 'editTour', component: EditTourComponent },
  { path: 'confirmation', component: ReservationConfirmationComponent }, 
  { path: 'myaccount', component: MyaccuntComponent },

];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    TourComponent,
    FooterComponent,
    TourListComponent,
    ShoppingCartComponent,
    AddTourComponent,
    ModalInfoComponent,
    ModalInfoCartComponent,
    CommentComponent,
    LoginComponent,
    RegisterComponent,
    FiltersComponent,
    PriceFilterComponent,
    DateFilterComponent,
    MarkFilterComponent,
    TourCountryFilterComponent,
    AdminPanelComponent,
    EditTourComponent,
    ReservationConfirmationComponent,
    OrderHistoryComponent,
    MyaccuntComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
   MDBBootstrapModule.forRoot(),
   HttpClientModule,
   NgxPaginationModule, 
   RouterModule.forRoot(
    appRoutes,
    { enableTracing: false }),
    NgbModule , 
    FormsModule,
    ReactiveFormsModule 
  
  ],
  providers: [ShoppingCartService, TourService,UserService, AuthorizationService, OrderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
