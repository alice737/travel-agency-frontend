export class User{
    constructor( 
        public name: string, 
        public surname: string, 
        public email: string, 
        public password: string,
        public ordersIds: any[],
        public cartToursIds: any[],
        public role:string){}
}
export class Login {
    constructor(public email: string,
                public password: string) {
    }
  }