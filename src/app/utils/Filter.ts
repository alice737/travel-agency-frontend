export class Filter{
    constructor(  public priceFrom:number, 
        public priceTo:number, 
        public dateFrom: Date, 
        public dateTo: Date, 
        public markFrom:number,
        public markTo:number, 
        public tourCity: string ) {}
}
