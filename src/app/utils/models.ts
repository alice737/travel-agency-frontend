import { Observable } from 'rxjs';

export interface Tour {
    _id?: number;
    imageSrc: string;
    cityName: string;
    countryName: string;
    hotelName: string;
    description: string; 
    price: number;
    startDate: Date,
    endDate: Date,
    numberSeats: number,
    numberReservedSeats: number,
    starCapacity: number, 
    comments:any[]
}
 
export interface Filters{
    priceFrom:number, 
    priceTo:number, 
    dateFrom: Date, 
    dateTo: Date, 
    markFrom:number, 
    markTo:number, 
    toursCountry: string 
}
export interface User{
    name: string, 
    surname: string, 
    email: string, 
    password: string,
    ordersIds: any[],
    cartToursIds: any[],
    role: string
}

export interface Order{
    email: string,
    date: Date,
    orderedToursIds: Tour[],
    isCompleted : Boolean
}
export interface Attraction {
    id?: number;
     name:string;
     address: string;
      lat:number;
      lng: number;
     city:string;
     country:string;
 imageLink:string;
}
