export class Api {
    
    private static BASE_END_POINT = 'http://localhost:3000/';
    static TOURS = Api.BASE_END_POINT + 'tours';
    static USER = Api.BASE_END_POINT + 'register';
    static ADDTOCART = Api.BASE_END_POINT + 'addToCart';
    static GETONEUSER = Api.BASE_END_POINT + 'getOne';
    static UPDATEUSER = Api.BASE_END_POINT + 'updateUser';
    static LOGIN = Api.BASE_END_POINT + 'login';
    static ORDER = Api.BASE_END_POINT + 'order';

}