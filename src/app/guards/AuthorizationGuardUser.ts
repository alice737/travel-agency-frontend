import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthorizationService} from "../services/authorization.service";


@Injectable()
export class  AuthorizationGuardUser implements CanActivate {
  constructor(private authorizationService: AuthorizationService,
              private router: Router) {

  }

  canActivate() {
    if (this.authorizationService.isLoggedInUser()) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
